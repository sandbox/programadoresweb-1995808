<?php

/**
 * Implementation of hook_token_list().
 */
function _token_example_token_list($type = 'all') {
  $tokens = array();

  if ($type == 'node' || $type == 'all') {

    $tokens['node']['parent-term']  = t("Taxonomy parent for the node.");
    $tokens['node']['child-term']  = t("Taxonomy child for the node.");
  }


  return $tokens;
}

/**
 * Implementation of hook_token_values().
 */
function _token_example_token_values($type, $object = NULL) {
  $values = array();

  // Current taxonomy
  $taxonomies = array_values($object->taxonomy);

  // if array not emtpy.
  if(!empty($taxonomies[1])){
    $parent_taxonomy = $taxonomies[0];
    $child_taxonomy = $taxonomies[1];

  } else {
    // Parent taxonomy.
    $parent_taxonomy = array_values(taxonomy_get_parents($taxonomies[0]->tid));
    $parent_taxonomy = $parent_taxonomy[0];

    $child_taxonomy = $taxonomies[0];

    if(empty($parent_taxonomy)){
      $parent_taxonomy = $taxonomies[0];
      $child_taxonomy = null;
    }

  }


  $values['parent-term'] = $parent_taxonomy->name;
  $values['child-term'] = $child_taxonomy->name;


  return $values;
}
